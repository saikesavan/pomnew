package week3.day2;
import java.util.HashMap;
import java.util.Map;

public class UniqueCharacters {


		public void unique(String s) {

			Map <Character,Integer> m = new HashMap<>();
			char[] c = s.toCharArray();

			for (char d : c) {
				if (m.containsKey(d)) {
					m.put(d, m.get(d)+1);	
				}else {
					m.put(d, 1);
				}
			}

			for (Map.Entry<Character, Integer> map: m.entrySet()) {
				if (map.getValue()==1) {
					System.out.println(map.getKey());
				}
			}
		}



		public static void main(String[] args) {
			// TODO Auto-generated method stub
			String s = "paypal india";
			UniqueCharacters f = new UniqueCharacters();
			f.unique(s);
		}

	}



