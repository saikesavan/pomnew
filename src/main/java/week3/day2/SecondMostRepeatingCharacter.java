package week3.day2;

import java.util.ArrayList;
import java.util.Collections;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SecondMostRepeatingCharacter {

	public void second(String s) {
		List<Character> lc = new ArrayList<>();
		List<Integer> li = new ArrayList<>();
		char[] c = s.toCharArray();
		int counter=0;
		for (char char1 : c) {
			lc.add(char1);
		}

		Set<Character> sc = new HashSet<>();
		sc.addAll(lc);


		//get the count of repetitions for each character and store it in set 
		Set<Integer> si = new HashSet<>();
		for (Character setc: sc) {
			counter = 0;
			for (Character lisc: lc) {
				if (setc.equals(lisc)){
					counter++;
				}
			}
			si.add(counter);
		}

		li.addAll(si);

		Collections.sort(li);

		System.out.println("Second most repeated character(s):");
		for (Character setc: sc) {
			counter = 0;
			for (Character lisc: lc) {
				if (setc.equals(lisc)){
					counter++;
				}
			}
			if (counter==li.get(li.size()-2)) 
			{
				System.out.println(setc);

			}

		}
		System.out.println("Number of times repeated:");
		System.out.println(li.get(li.size()-2));

	}


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SecondMostRepeatingCharacter sc = new SecondMostRepeatingCharacter();
		String s = "amazon india  private limited";
		sc.second(s);
	}
}
