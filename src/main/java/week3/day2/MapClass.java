package week3.day2;

import java.util.HashMap;

import java.util.Map;

public class MapClass {
	
	
	public void addToMap(String s) {
		
	Map<Character,Integer> m = new HashMap<>();
	char[] c = s.toCharArray();
	
	for (char char1 : c) {
		if(m.containsKey(char1)) {
		m.put(char1, m.get(char1)+1);
		}
		else {
		m.put(char1,1);
		}
		
	}
	
	for (Map.Entry<Character, Integer> map: m.entrySet()) {
		System.out.println(map.getKey()+" "+map.getValue());
		
	}
	
	} 

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String s = "WELCOME TO AMAZON";
		MapClass map1 = new MapClass();
		map1.addToMap(s);
	}

}
