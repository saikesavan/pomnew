package week3.day2;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;



public class ListandSet {

	public void SetImplementation()
	{

		Set<String> st = new HashSet<>();
		st.add("Gita");
		st.add("Shane");
		st.add("Sam");
		st.add("Gita");
		System.out.println("Set before converting to list doesn't print in order:");
		for (String set :  st) {

			System.out.println(set);
		}
		List<String> ls = new ArrayList<>();
		ls.addAll(st);
		System.out.println("Set converted to List prints in order");
		for (String startwithS :  ls) {

			System.out.println(startwithS);
		}
		System.out.println("Names starting with S:");
		for (String startwithS1 :  ls) {
			if(startwithS1.startsWith("S")) {
				System.out.println(startwithS1);
			}
		}
		ls.add("Gita");
		ls.add("Gita");
		ls.add("Sam");


		Collections.sort(ls);
		System.out.println("sorted list:");	

		for (String list :  ls) {

			System.out.println(list);

		}	

		int count =0;
		System.out.println("Duplicates");
		for (String eachsetname : st) {
			count = 0;
			for (String eachlistname: ls) {
				if (eachsetname.equals(eachlistname)) {
					count++;
				}
			}
			if (count>1) {
				System.out.println(eachsetname);
			}
		}    

	}


	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ListandSet lst = new ListandSet();
		lst.SetImplementation();

	}



}
