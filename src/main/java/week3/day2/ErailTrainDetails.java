package week3.day2;



import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class ErailTrainDetails {


	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "C:\\TestLeaf\\Maven\\MavenProject\\drivers\\chromedriver.exe");
		ChromeDriver d = new ChromeDriver();
		d.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		d.get("https://erail.in");
		d.findElementById("txtStationFrom").clear();
		d.findElementById("txtStationFrom").sendKeys("MS",Keys.TAB,Keys.TAB);
		d.findElementById("txtStationTo").sendKeys("TPJ",Keys.TAB);
		d.findElementById("chkSelectDateOnly").click();

		//get the values from column #1 train number from table
		List<WebElement> tl = d.findElementsByXPath("//table[@class='DataTable TrainList TrainListHeader']/tbody/tr/td[1]");

		//add those values to the list and sort them
		List<String> sortList = new ArrayList<>();
		for (WebElement webElement : tl) {
			sortList.add(webElement.getText());
		}
		Collections.sort(sortList);
		System.out.println("Sorted List:");
		for (String string : sortList) {
			System.out.println(string);
		}

		//click on train header to sort
		d.findElementByLinkText("Train").click();

		//	verify sorting is working
		List<WebElement> tl2 = d.findElementsByXPath("//table[@class='DataTable TrainList TrainListHeader']/tbody/tr/td[1]");

		List<String> trainName = new ArrayList<>();

		for (WebElement webElement : tl2) {
			trainName.add(webElement.getText());
		}

		if(sortList.equals(trainName)){
			System.out.println("Sort functionality is working as expected");
		}

	}}
