package week2.day2;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ListWebElements {


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver d = new ChromeDriver();
		d.get("http://leafground.com/pages/table.html");

		//click on the last checkbox
		List<WebElement> listCount = d.findElementsByXPath("//input[@type='checkbox']");
		d.findElementsByXPath("//input[@type='checkbox']").get(listCount.size()-1).click();
		//find number of rows
		List<WebElement> tr  = d.findElementsByTagName("tr");
		System.out.println(tr.size());
		
		//print progress
		for (int i = 0;i<tr.size()-1;i++) {
		System.out.println(d.findElementsByTagName("td").get(i).getText());	
		}
	 
		
	}}
