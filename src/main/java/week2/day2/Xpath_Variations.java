package week2.day2;



import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;




public class Xpath_Variations {


	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver d = new ChromeDriver();
		d.get("http://leaftaps.com/opentaps/");
		d.findElementById("username").sendKeys("DemoSalesManager");
		d.findElementById("password").sendKeys("crmsfa");
		String title = d.getTitle();
		System.out.println(title);
		d.findElementByClassName("decorativeSubmit").click();
		d.findElementByLinkText("CRM/SFA").click();
		d.findElementByLinkText("Create Lead").click();
		d.findElementByLinkText("Find Leads").click();

		d.findElement(By.xpath("//label[text()='Lead ID:']/following::input")).sendKeys("1001");
		d.findElement(By.xpath("//button[text()='Find Leads']")).click();
		String a = d.findElement(By.xpath("//label[text()='Lead ID:']/following::input")).getAttribute("value");

		Thread.sleep(1000);
		System.out.println(a);
		d.findElement(By.xpath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a")).click();
		//String text = d.findElement(By.xpath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a")).getText();
		//System.out.println(text);
		//if (text.equals(a)) {
		//System.out.println("lead id matches");
		//}

	}
}