package week2.day2;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import week5.day1.ProjectBase;

public class DeleteLead extends ProjectBase{
	@BeforeTest
	public void setData() {
		excelFileName = "AllLeadTests";
		excelSheetName = "deleteTestLead";
	}
	
	
	@Test (dataProvider = "fetchData")
	
	//@Test(dependsOnMethods = {"week2.day2.CreateLead.runcreateLead","week2.day2.EditLead.runEditLead"})
	public void runDeleteLead(String phone) throws InterruptedException {
		
		String title = driver.getTitle();
		System.out.println(title);
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("(//a[@class='x-tab-right'])[2]").click();
		driver.findElementByName("phoneNumber").sendKeys(phone);
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(5000);
		String leadId = driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").getText();
		driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").click();
		driver.findElementByLinkText("Delete").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("//label[text()='Lead ID:']/following::input").sendKeys(leadId);
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(5000);
		if (driver.findElementByXPath("//div[text()='No records to display']").getText().equals("No records to display")) {
			System.out.println("Lead Id: "+leadId+" Deleted Successfully");}
		

		}
		

		}

