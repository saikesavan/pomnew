package week2.day2;


import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import week5.day1.ProjectBase;

public class EditLead extends ProjectBase{
	@BeforeTest
	public void setData() {
		excelFileName = "AllLeadTests";
		excelSheetName = "editTestLead";
	}
	
	
	@Test (dataProvider = "fetchData")
	public void runEditLead (String fName, String cName) throws InterruptedException {
		String title = driver.getTitle();
		System.out.println(title);
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("(//label[text()='First name:'])[3]/following::input").sendKeys(fName);
		driver.findElementByXPath("//button[text()='Find Leads']").click();
	//	WebDriverWait w = new WebDriverWait(d,5);
	//	w.until(ExpectedConditions.elementToBeClickable(d.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a")));
		Thread.sleep(5000);
		System.out.println(driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a").getText());
		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a").click();
		
		System.out.println(driver.getTitle());
		String previous_company = driver.findElementById("viewLead_companyName_sp").getText();
		System.out.println("Previous Company Name is: "+previous_company);
		driver.findElementByLinkText("Edit").click();
		driver.findElementById("updateLeadForm_companyName").clear();
		String a = cName;
		driver.findElementById("updateLeadForm_companyName").sendKeys(a);
		driver.findElementByXPath("//textarea[@id='updateLeadForm_importantNote']/following::input").click();
		String company_name = driver.findElementById("viewLead_companyName_sp").getText();

		if (company_name.substring(0, a.length()).equals(a)){
			System.out.println("New Company Name is: "+ company_name.substring(0, a.length())+" Company Name Matches; Update Successful");}
		else {
			System.out.println("Company Name Matches; Update Not Successful");
		}
	

	}

}

