package week2.day2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class DragandDrop {





		public static void main(String[] args) {
			System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
			ChromeDriver driver = new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
			
			driver.get("http://jqueryui.com/droppable/");
			driver.switchTo().frame(0);
			WebElement source = driver.findElementByXPath("//p[text()='Drag me to my target']");
		    Actions builder = new Actions((WebDriver) driver);
			WebElement target = driver.findElementByXPath("//p[text()='Drop here']");
			WebDriverWait wWait = new WebDriverWait(driver, 20);
			wWait.until(ExpectedConditions.elementToBeClickable(source));
			builder.dragAndDrop(source, target).perform();
		
		}
}