package week2.day2;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import week5.day1.ProjectBase;

public class DuplicateLead extends ProjectBase{
	@BeforeTest
	public void setData() {
		excelFileName = "AllLeadTests";
		excelSheetName = "duplicateTestLead";
	}


	@Test (dataProvider = "fetchData")
	public void runDuplicateLead(String mailId) throws InterruptedException {
		String title = driver.getTitle();
		System.out.println(title);
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("(//a[@class='x-tab-right'])[3]").click();
		driver.findElementByName("emailAddress").sendKeys(mailId);
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(5000);
		String leadName = driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-firstName']/a").getText();
		System.out.println("Original Lead Name: "+leadName);
		driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-firstName']/a").click();
		driver.findElementByLinkText("Duplicate Lead").click();
		if(driver.getTitle().equals("Duplicate Lead")) {
			System.out.println("Title is: Duplicate Lead");
		}

		driver.findElementByXPath("//input[@id='createLeadForm_generalPostalCodeExt']/following::input").click();
		if (leadName.equals(driver.findElementById("viewLead_firstName_sp").getText())) {
			System.out.println("Duplicate Lead Name: "+driver.findElementById("viewLead_firstName_sp").getText());
		}


	}}