package week2.day2;


//import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.interactions.Actions;
//import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeTest;
//import org.testng.annotations.DataProvider;
//import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import week5.day1.ProjectBase;


public class CreateLead extends ProjectBase{

	@BeforeTest
	public void setData() {
		excelFileName = "AllLeadTests";
		excelSheetName = "createTestLead";
	}

	@Test (dataProvider = "fetchData")
	public void runcreateLead(String cName, String fName, String  lName) {
		System.setProperty("webdriver.chrome.driver", "C:\\TestLeaf\\Maven\\MavenProject\\drivers\\chromedriver.exe");
		String title = driver.getTitle();
		System.out.println(title);
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys(cName);
		driver.findElementById("createLeadForm_companyName").sendKeys(Keys.TAB);
		driver.findElementById("createLeadForm_parentPartyId").sendKeys("accountlimit100");	

		//d.findElementByXPath("(//input[@id='createLeadForm_parentPartyId'])/following::img").click();

		/*	WebElement move1 = driver.findElementByXPath("(//input[@id='createLeadForm_parentPartyId'])/following::img");
		WebDriverWait wWait = new WebDriverWait(driver, 20);
		wWait.until(ExpectedConditions.elementToBeClickable(move1));
		Actions builder = new Actions(driver);
		builder.moveToElement(move1).click().perform();

    driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a").click();*/
		//	WebDriverWait wWait1 = new WebDriverWait(d, 20);
		//	wWait1.until(ExpectedConditions.elementToBeClickable(move2));
		//Actions builder1 = new Actions(d);
		//builder1.moveToElement(move2).click().perform();


		driver.findElementById("createLeadForm_firstName").sendKeys(fName);
		driver.findElementById("createLeadForm_lastName").sendKeys(lName);
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("s");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("n");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("Mr");
		driver.findElementById("createLeadForm_birthDate").sendKeys("01/27/86");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("100000");
		WebElement c = driver.findElementById("createLeadForm_currencyUomId");
		Select curr = new Select(c);
		curr.selectByValue("INR");

		WebElement i = driver.findElementById("createLeadForm_industryEnumId");
		Select ind = new Select(i);
		ind.selectByVisibleText("Finance");

		WebElement o = driver.findElementById("createLeadForm_ownershipEnumId");
		Select own = new Select(o);
		own.selectByValue("OWN_PARTNERSHIP");

		driver.findElementById("createLeadForm_sicCode").sendKeys("001");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("TL");
		driver.findElementById("createLeadForm_description").sendKeys("Test Leaf institute");
		driver.findElementById("createLeadForm_importantNote").sendKeys("Test Leaf i");
		driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("91");

		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("044");
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("421301");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("324");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("Raghu");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("sainath.kesavan@gmail.com");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("http://www.google.com");
		driver.findElementById("createLeadForm_generalToName").sendKeys("Stephan");
		driver.findElementById("createLeadForm_generalAttnName").sendKeys("Stella");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("2nd Gandhi Street");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("Adyar");
		driver.findElementById("createLeadForm_generalCity").sendKeys("Chennai");

		WebElement co = driver.findElementById("createLeadForm_generalCountryGeoId");
		Select coun = new Select(co);
		coun.selectByValue("IND");

		WebElement g = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
		Select state = new Select(g);
		state.selectByValue("IN-TN");

		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("60023");
		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("21");
		driver.findElementByXPath("(//input[@id='createLeadForm_generalPostalCodeExt'])/following::input").click();

	}
	/*	 @DataProvider(name = "fetchData" , parallel = true)
	public String[][] sendData(){
		String[][] data = new String[3][3];
		data[0][0] = "Test Leaf";
		data[0][1] = "Sai";
		data[0][2] = "Nair";

		data[1][0] = "Acc";
		data[1][1] = "Santha";
		data[1][2] = "Shenoy";

		data[2][0] = "CTS";
		data[2][1] = "Raghu";
		data[2][2] = "M";


		return data;
	}*/

}
