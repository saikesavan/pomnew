package week2.day1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IrctcAutomation {	
	public static void main(String[] args) {
	
	System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
	ChromeDriver d = new ChromeDriver();
	d.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
	d.findElementById("userRegistrationForm:userName").sendKeys("saikesavan");
	d.findElementById("userRegistrationForm:password").sendKeys("Sai4123!");
	WebElement dd1 = d.findElementById("userRegistrationForm:securityQ");
	Select securityQuestion = new Select(dd1);
	securityQuestion.selectByIndex(2);
	d.findElementById("userRegistrationForm:securityAnswer").sendKeys("jimmy");
	WebElement dd2 = d.findElementById("userRegistrationForm:prelan");
	Select preferredLanguage = new Select(dd2);
	preferredLanguage.selectByValue("hi");	
	d.findElementById("userRegistrationForm:firstName").sendKeys("Sai");
	d.findElementById("userRegistrationForm:middleName").sendKeys("K");
	d.findElementById("userRegistrationForm:lastName").sendKeys("Nair");	
	d.findElement(By.xpath("(//input[@value = 'M'])[1]")).click();
	d.findElement(By.xpath("(//input[@value = 'M'])[2]")).click();

	WebElement dob1 = d.findElementById("userRegistrationForm:dobDay");
	Select dobDay = new Select(dob1);
	dobDay.selectByVisibleText("27");
	
	WebElement dob2 = d.findElementById("userRegistrationForm:dobMonth");
	Select dobMonth = new Select(dob2);
	dobMonth.selectByVisibleText("JAN");
	
	WebElement dob3 = d.findElementById("userRegistrationForm:dateOfBirth");
	Select dobYear = new Select(dob3);
	dobYear.selectByVisibleText("1986");
	
	WebElement occ = d.findElementById("userRegistrationForm:occupation");
	Select occupation = new Select(occ);
	occupation.selectByVisibleText("Public");
	
	WebElement cntry = d.findElementById("userRegistrationForm:occupation");
	Select country = new Select(cntry);
	country.selectByVisibleText("India");
	
	d.findElementById("userRegistrationForm:email").sendKeys("saikesavan");
	String isdCode = d.findElementById("userRegistrationForm:isdCode").getAttribute("value");
	if (isdCode.equals("91")){
		System.out.println("valid isd code");
	}
	
	
	
	}
	

}
