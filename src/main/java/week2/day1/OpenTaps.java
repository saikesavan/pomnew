package week2.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class OpenTaps {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver d = new ChromeDriver();
		d.get("http://leaftaps.com/opentaps/");
		d.findElementById("username").sendKeys("DemoSalesManager");
		d.findElementById("password").sendKeys("crmsfa");
		String title = d.getTitle();
		System.out.println(title);
		d.findElementByClassName("decorativeSubmit").click();
		d.findElementByLinkText("CRM/SFA").click();
		d.findElementByLinkText("Create Lead").click();
		d.findElementById("createLeadForm_companyName").sendKeys("Test Leaf");
		d.findElementById("createLeadForm_firstName").sendKeys("Sai");
		d.findElementById("createLeadForm_lastName").sendKeys("Nair");

		//d.findElementByName("submitButton").click();
		//title = d.getTitle();
		//System.out.println(title);
		//String text = d.findElementById("viewLead_companyName_sp").getText();
		//System.out.println(text);
		//d.close();

		//above lines are valid commented for select option

		WebElement dd1 = d.findElementById("createLeadForm_dataSourceId");
		Select dropdown = new Select(dd1);
		int size = dropdown.getOptions().size();
		dropdown.selectByIndex(size-1);

		WebElement dd2 = d.findElementById("createLeadForm_marketingCampaignId");
		Select dropdown1 = new Select(dd2);
		dropdown1.selectByVisibleText("Car and Driver");

	}

}

