Feature: Leads_All Actions
Background:
Given Open the Chrome Browser
And Maximise the Browser
And Set the Timeouts
And Load the URL
And Enter the username as DemoSalesManager
And Enter the password as crmsfa
And Click on Login Button
And Login is Successful
And Click on CRM-SFA
And Click on Leads Tab

Scenario Outline: TC001_Create Lead Flow
Given Click on Create Lead
And Enter the company name as <company_name>
And Enter the first name as <first_name>
And Enter the last name as <last_name>
When Click on Create Lead Button
Then Lead Created Successfully
And Take Snapshot as Scenario

Examples:
|company_name|first_name|last_name|
|CapGemini|Shaanu|Thakur|

Scenario Outline: TC002_Delete_Lead_Flow
Given Click on Find Leads Tab
And Click on Phone Tab 
And Enter the Phone number as <phone_number>
And Click on Find Leads button
And Leads with given phone number are displayed
And Click on the first displayed Lead Id
And Click on Delete Button	
And Click on Find Leads Tab 
And Enter the Lead Id
When Click on Find Leads button
Then No records to display message is displayed
And Take Snapshot as Scenario

Examples:
|phone_number|
|9|

Scenario Outline: TC003_Edit_Lead_Flow
Given Click on Find Leads Tab
And Enter the First Name as <first_name>
And Click on Find Leads button
And Click on the first displayed Lead Id
And Observe the Company Name
And Click on Edit Button
And Update the Company Name as <company_name>
When Click on Update Button
Then Validate Updated Company is displayed
And Take Snapshot as Scenario

Examples:
|first_name|company_name|
|asha|Accenture|

Scenario Outline: TC004_Duplicate_Lead_Flow
Given Click on Find Leads Tab
And Enter the First Name as <first_name>
And Click on Find Leads button
And Note the First Name
And Click on the first displayed Lead Id
And Click on Duplicate Button
And Validate the Duplicate Lead screen is displayed
When Click on the Update Button
Then Validate Duplicate First Name is displayed
And Take Snapshot as Scenario

Examples:
|first_name|
|sai|

Scenario: TC005_Merge_Lead_Flow
Given Click on Merge Leads Tab
And Click the from lead icon
And Switch to next screen
And Select the first visible lead
And Switch back to previous screen
When Merge Leads
Then Validate Merge is successful
And Take Snapshot as Scenario
