package week5.day1;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import week5.day2.Excel;



public class ProjectBase {

	public String excelFileName;
	public String excelSheetName;
	public ChromeDriver driver;


	@Parameters ({"username","password"})

	@BeforeMethod
	public void launchApp(String uname, String pwd) {

		System.setProperty("webdriver.chrome.driver", "C:\\TestLeaf\\Maven\\MavenProject\\drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/");
		driver.findElementById("username").sendKeys(uname);
		driver.findElementById("password").sendKeys(pwd);

	}




	@AfterMethod
	public void CloseBrowser() {
		driver.quit();
	}

	@DataProvider(name ="fetchData", parallel = true)
	public String[][] sendData() throws IOException{

		Excel excel = new Excel();
		return excel.readExcel(excelFileName,excelSheetName);
	}
}

