package week5.day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel  {
	public String[][] readExcel (String excelFileName, String excelSheetName) throws IOException {
		
		XSSFWorkbook wBook = new XSSFWorkbook("./test data/"+excelFileName+".xlsx");
		
		String[][] data;
		
		XSSFSheet sheet = wBook.getSheet(excelSheetName);
		int lastRowNum = sheet.getLastRowNum();
		int lastCellNum = sheet.getRow(0).getLastCellNum();
		data = new String[lastRowNum][lastCellNum];
        	
		for (int i = 1;i<=lastRowNum;i++) {
			XSSFRow row = sheet.getRow(i);
			for (int j=0;j<lastCellNum;j++) {
				XSSFCell cell = row.getCell(j);
				String cellValue = cell.getStringCellValue();
                data[i-1][j]=cellValue;
	//			System.out.print(cellValue+"	");
			}
		/*	System.out.println();
			if (i==0) {
				System.out.println("==========================");
			}
			if (i!=0) {
	
			System.out.println("Value fetched from row "+row.getRowNum());
			System.out.println("==========================");
			}*/
		}
		wBook.close();
		return data;	
		}
	
	}

