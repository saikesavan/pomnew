package week1.day2;

public class int_array_sort {
	
	public void sort(int a[]) {
		
		int b = 0;
		for (int i=0; i<a.length-1;i++) {
			for (int j=i+1;j<a.length;j++) {
				if (a[i]>a[j]) {
				b=a[i];
				a[i]=a[j];
				a[j]=b;
				}
			}
		}
		System.out.println("Sorted Integer Array:");
		for(int eachnumber: a) {
	    	System.out.println(eachnumber);
	    	}
	}
	
	public static void main(String[] args) {
	int_array_sort i = new int_array_sort();
	int[] j = {48,12,15,43,32,10,0};
	System.out.println("Input Integer Array:");
	for (int s: j) {
		System.out.println(s);
	}
	i.sort(j);
	}

}
