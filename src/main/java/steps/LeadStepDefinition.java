package steps;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
//import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import cucumber.api.CucumberOptions;
import cucumber.api.Result.Type;
import cucumber.api.Scenario;
import cucumber.api.SnippetType;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

@CucumberOptions(features = "src/main/java/features", glue = "steps", dryRun = false, snippets = SnippetType.CAMELCASE)

public class LeadStepDefinition {
	public ChromeDriver driver;
	public String leadId;
	public String previous_company;
	public String comp_name;
	public String leadName;
	public List<String> getWindow;
	public String mergeFrom;
	public String mergeTo;
    public String name;
	@Before
	public void beforeScenario(Scenario sc) {
		name = sc.getName();
		System.out.println(name);
		String id = sc.getId();
		System.out.println(id);
	}

	@After
	public void afterScenario(Scenario sc) {
		Type status = sc.getStatus();
		System.out.println(status);
		driver.quit();
	}

	@Given("Open the Chrome Browser")
	public void openTheChromeBrowser() {
		System.setProperty("webdriver.chrome.driver", "C:\\TestLeaf\\Maven\\MavenProject\\drivers\\chromedriver.exe");
		driver = new ChromeDriver();

	}

	@And("Maximise the Browser")
	public void maximiseTheBrowser() {
		driver.manage().window().maximize();

	}

	@And("Set the Timeouts")
	public void setTheTimeouts() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}

	@And("Load the URL")
	public void loadTheURL() {
		driver.get("http://leaftaps.com/opentaps/");

	}

	@And("Enter the username as (.*)")
	public void enterTheUsernameAsDemoSalesManager(String uname) {
		driver.findElementById("username").sendKeys(uname);
	}

	@And("Enter the password as (.*)")
	public void enterThePasswordAsCrmsfa(String pwd) {
		driver.findElementById("password").sendKeys(pwd);
	}

	@When("Click on Login Button")
	public void clickOnLoginButton() {
		driver.findElementByClassName("decorativeSubmit").click();

	}

	@And("Login is Successful")
	public void loginIsSuccessful() {
		System.out.println("Login is successful");
	}

	@And("Click on CRM-SFA")
	public void crmSFAclick() {
		driver.findElementByLinkText("CRM/SFA").click();
	}

	@And("Click on Leads Tab")
	public void clickLeadsTab() {
		driver.findElementByLinkText("Leads").click();
	}

	@Given("Click on Create Lead")
	public void clickCreateLead() {
		driver.findElementByLinkText("Create Lead").click();
	}

	@And("Enter the company name as (.*)")
	public void enterTheCompanyName(String cName) {
		driver.findElementById("createLeadForm_companyName").sendKeys(cName);
	}

	@And("Enter the first name as (.*)")
	public void enterTheFirstName(String fName) {
		driver.findElementById("createLeadForm_firstName").sendKeys(fName);

	}

	@And("Enter the last name as (.*)")
	public void enterTheLastName(String lName) {
		driver.findElementById("createLeadForm_lastName").sendKeys(lName);
	}

	@When("Click on Create Lead Button")
	public void clickOnCreateLeadButton() {
		driver.findElementByXPath("(//input[@id='createLeadForm_generalPostalCodeExt'])/following::input").click();
	}

	@Then("Lead Created Successfully")
	public void leadCreatedSuccessfully() {
		// Write code here that turns the phrase above into concrete actions
		System.out.println("Lead Created Successfully");
	}

	// Delete Lead feature file step definition

	@And("Click on Find Leads Tab")
	public void clickFindLeadsTab() {

		driver.findElementByLinkText("Find Leads").click();
	}

	@And("Click on Phone Tab")
	public void clickOnPhoneTab() {
		driver.findElementByXPath("(//a[@class='x-tab-right'])[2]").click();
	}

	@And("Enter the Phone number as (.*)")
	public void phoneNumber(String phone) {
		driver.findElementByName("phoneNumber").sendKeys(phone);
	}

	@When("Click on Find Leads button")
	public void clickFindLeadsButton() throws InterruptedException {
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(5000);
	}

	@Then("Leads with given phone number are displayed")
	public void leadsWithGivenPhoneNumber() {
		leadId = driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").getText();

	}

	@Given("Click on the first displayed Lead Id")
	public void firstDisplayedLead() {
		driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").click();
	}

	@And("Click on Delete Button")
	public void deleteButton() {
		driver.findElementByLinkText("Delete").click();
	}

	@And("Enter the Lead Id")
	public void enterLeadId() {
		driver.findElementByXPath("//label[text()='Lead ID:']/following::input").sendKeys(leadId);
	}

	@Then("No records to display message is displayed")
	public void messageDisplayed() {
		if (driver.findElementByXPath("//div[text()='No records to display']").getText()
				.equals("No records to display")) {
			System.out.println("Lead Id: " + leadId + " Deleted Successfully");
		}
	}

	// Edit Lead step definition

	@And("Enter the First Name as (.*)")
	public void enterFirstName(String fName) {
		driver.findElementByXPath("(//label[text()='First name:'])[3]/following::input").sendKeys(fName);
	}

	@And("Observe the Company Name")
	public void previousCompanyName() {
		previous_company = driver.findElementById("viewLead_companyName_sp").getText();
		System.out.println("Previous Company Name is: " + previous_company);
	}

	@And("Click on Edit Button")
	public void clickEditButton() {
		driver.findElementByLinkText("Edit").click();
	}

	@And("Update the Company Name as (.*)")
	public void updateCompanyName(String cName) {
		comp_name = cName;
		driver.findElementById("updateLeadForm_companyName").clear();
		driver.findElementById("updateLeadForm_companyName").sendKeys(cName);
	}

	@When("Click on Update Button")
	public void clickUpdateButton() {
		driver.findElementByXPath("//textarea[@id='updateLeadForm_importantNote']/following::input").click();
	}

	@Then("Validate Updated Company is displayed")
	public void validateUpdatedCompanyName() {
		String company_name = driver.findElementById("viewLead_companyName_sp").getText();
		if (company_name.substring(0, comp_name.length()).equals(comp_name)) {
			System.out.println("New Company Name is: " + company_name.substring(0, comp_name.length())
					+ " Company Name Matches; Update Successful");
		} else {
			System.out.println("Company Name Matches; Update Not Successful");
		}
	}

	// Duplicate Lead Step Definition

	@And("Note the First Name")
	public void firstName() {
		leadName = driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-firstName']/a").getText();
		System.out.println("Original Lead Name: " + leadName);
	}

	@And("Click on Duplicate Button")
	public void clickDuplicateButton() {
		driver.findElementByLinkText("Duplicate Lead").click();
	}

	@And("Validate the Duplicate Lead screen is displayed")
	public void validateDuplicateLeadScreen() {
		if (driver.getTitle().equals("Duplicate Lead")) {
			System.out.println("Title is: Duplicate Lead");
		}
	}

	@When("Click on the Update Button")
	public void clickUpdate() {
		driver.findElementByXPath("//input[@id='createLeadForm_generalPostalCodeExt']/following::input").click();
	}

	@Then("Validate Duplicate First Name is displayed")
	public void validateDuplicateFirstName() {
		if (leadName.equals(driver.findElementById("viewLead_firstName_sp").getText())) {
			System.out.println("Duplicate Lead Name: " + driver.findElementById("viewLead_firstName_sp").getText());
		}
	}

	// merge leads step definition
	@Given("Click on Merge Leads Tab")
	public void clickMergeLeads() {
		driver.findElementByLinkText("Merge Leads").click();
	}

	@And("Click the from lead icon")
	public void clickFromLead() {
		WebElement fromLead = driver.findElementByXPath("//input[@id = 'partyIdFrom']/following::img");
		Actions builder = new Actions(driver);
		builder.click(fromLead).perform();
	}

	@And("Switch to next screen")
	public void switchToNextScreen() {
		Set<String> allWindowHandles = driver.getWindowHandles();
		getWindow = new ArrayList<>();
		getWindow.addAll(allWindowHandles);
		driver.switchTo().window(getWindow.get(1));
		System.out.println(driver.getTitle());
	}

	@And("Select the first visible lead")
	public void selectFirstVisibleLead() throws InterruptedException {
		mergeFrom = driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a")
				.getText();
		mergeTo = driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[2]/a").getText();
		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a").click();
	}

	@And("Switch back to previous screen")
	public void switchBackToPreviousScreen() {
		driver.switchTo().window(getWindow.get(0));
	}

	@When("Merge Leads")
	public void mergeLeads() {
		driver.findElementByXPath("(//input[@class='XdijitInputField dijitInputFieldValidationNormal'])[2]")
				.sendKeys(mergeTo);
		driver.findElementByLinkText("Merge").click();
		driver.switchTo().alert().accept();
		driver.switchTo().defaultContent();
	}

	@Then("Validate Merge is successful")
	public void validateMergeSuccess() {
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("//label[text()='Lead ID:']/following::input").sendKeys(mergeFrom);
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		if (driver.findElementByXPath("//div[text()='No records to display']").isDisplayed()) {
			System.out.println("validation successful");
		}
	}

	@And("Take Snapshot as (.*)")
	public void takeSnapshot(String snapShot) throws IOException {
		snapShot = name;
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-mm-yy-hh-mm-ss");
	   String dateTime = LocalDateTime.now().format(dateTimeFormatter);
	   System.out.println(dateTime);
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dest = new File("./snapshots/" + snapShot +dateTime+".jpg");
		FileUtils.copyFile(src, dest);

	}
}
