package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import week5.day1.ProjectBase;

public class MergeLeadsGetWindow extends ProjectBase{
	@BeforeTest
	public void setData() {
		excelFileName = "AllLeadTests";
		excelSheetName = "mergeTestLead";
	}


	@Test (dataProvider = "fetchData")
	public void runMergeLeads(String leadId) throws InterruptedException {
		String title = driver.getTitle();
		System.out.println(title);
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		//click the from lead icon
		WebElement fromLead = driver.findElementByXPath("//input[@id = 'partyIdFrom']/following::img");
		Actions builder = new Actions(driver);
		builder.click(fromLead).perform();
		//add the window handles to list and switch to next screen
		Set<String> allWindowHandles = driver.getWindowHandles();
		List<String> getWindow = new ArrayList<>();
		getWindow.addAll(allWindowHandles);
		driver.switchTo().window(getWindow.get(1));
		System.out.println(driver.getTitle());
		//select the first visible lead
		driver.findElementByXPath("//label[text()='Lead ID:']/following::input").sendKeys(leadId);
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		String mergeFrom =	driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a").getText();
		String mergeTo = driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[2]/a").getText();
		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a").click();

		//switch back to previous screen	
		driver.switchTo().window(getWindow.get(0));

		driver.findElementByXPath("(//input[@class='XdijitInputField dijitInputFieldValidationNormal'])[2]").sendKeys(mergeTo);
		driver.findElementByLinkText("Merge").click();

		driver.switchTo().alert().accept();
		driver.switchTo().defaultContent();		
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("//label[text()='Lead ID:']/following::input").sendKeys(mergeFrom);
		driver.findElementByXPath("//button[text()='Find Leads']").click();


		if (driver.findElementByXPath("//div[text()='No records to display']").isDisplayed())
		{

			System.out.println("validation successful");
		}
	}

}
