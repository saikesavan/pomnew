package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class PromptBox {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//driver.get("https://www.w3schools.com/js/js_popup.asp");
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");

		//	driver.findElementByXPath("(//a[text()='Try it Yourself �'])[3]").click();

		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text()='Try it']").click();

		Alert alert = driver.switchTo().alert();
		String s = "Sai Nair";
		alert.sendKeys(s);
		alert.accept();

		String contains = driver.findElementById("demo").getText();

		if (contains.contains(s)) {
			System.out.println("Prompt Box works");
		}

	}

}
