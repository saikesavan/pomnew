package week4.day2;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;


public class Facebook {

	public static void main(String[] args) {
		//disable the notification from Facebook on successful login
		ChromeOptions option = new ChromeOptions();
		option.addArguments("--disable-notifications");

		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver(option);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		//open url an login with credentials
		driver.get("https://www.facebook.com/");
		driver.findElementById("email").clear();
		driver.findElementById("email").sendKeys("sainath.kesavan@gmail.com");
		driver.findElementById("pass").sendKeys("ellavarumshowoff");
		driver.findElementById("loginbutton").click();

		//click on search textbox and enter search text and click on button
		driver.findElementByXPath("//div[@class='innerWrap']//input/following-sibling::input").click();
		driver.findElementByXPath("//div[@class='innerWrap']//input/following-sibling::input").sendKeys("Testleaf");
		driver.findElementByXPath("//div[@class='innerWrap']//preceding::button").click();
		
		//click on places 
		driver.findElementByXPath("//div[contains(@id,'u_fetchstream')]/div/div/div/ul/li[7]/a").click();

		//check if the like button is clicked. if not, click it.
		String likeText = driver.findElementByXPath("(//Button[contains(@class,'like')])[1]").getAttribute("class");

		if (likeText.contains("likeButton")) {
			driver.findElementByXPath("(//Button[contains(@class,'like')])[1]").click();
			System.out.println("Page Liked");
		} else {
			if (likeText.contains("likedButton")) {
				System.out.println("Page is already Liked");

			}
		}

		//check if test leaf link is displayed
		if (driver.findElementByLinkText("TestLeaf").isDisplayed()) {
			System.out.println(driver.findElementByLinkText("TestLeaf").getText()+" is the text displayed");
			driver.findElementByLinkText("TestLeaf").click();
		}

		//verify the title of the page
		String titleText = driver.findElementById("pageTitle").getText();
		if (titleText.contains("TestLeaf")) {
			System.out.println("Title contains TestLeaf");
		}

		//display the total number of likes for the page
		String totalLikes = driver.findElementByXPath("//div[contains(text(),'people like this')]").getText();
		String[] likeNumber = totalLikes.split(" ");
		System.out.println("Total number of likes are: "+likeNumber[0]);

	}

}
