package week4.day2;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Practice {

	public static void main(String[] args) {
		//disable the notification from Facebook on successful login
		ChromeOptions option = new ChromeOptions();
		option.addArguments("--disable-notifications");

		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver(option);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		//open url an login with credentials
		driver.get("https://www.facebook.com/");
		driver.findElementById("email").clear();
		driver.findElementById("email").sendKeys("sainath.kesavan@gmail.com");
		driver.findElementById("pass").sendKeys("ellavarumshowoff");
		driver.findElementById("loginbutton").click();
		
		//click on search textbox and enter search text and click on button
		driver.findElementByXPath("//div[@class='innerWrap']//input/following-sibling::input").click();
		driver.findElementByXPath("//div[@class='innerWrap']//input/following-sibling::input").sendKeys("Testleaf");
		driver.findElementByXPath("//div[@class='innerWrap']//preceding::button").click();


		//get the first values
	String x = driver.findElementByXPath("//div[contains(@id,'u_fetchstream')]/div/div/div/ul/li[last()-4]").getText();
	//	for (WebElement w : x) {
		System.out.println(x);
	//		}

	}

}
