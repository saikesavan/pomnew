package runner;

import cucumber.api.CucumberOptions;

import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features = "src/main/java/features/LeadAllActions.feature", glue = "steps", monochrome = true)
public class RunTest extends AbstractTestNGCucumberTests {

}
